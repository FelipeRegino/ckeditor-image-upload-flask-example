import os
import json
from flask import Flask, request, render_template

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def upload():
    try:
        file = request.files['upload']
        filename = file.filename
        file_path = os.path.join("static/", filename)
        file.save(file_path)
        response = {
            "uploaded": 1,
            "fileName": filename,
            "url": file_path
        }
    except:
        response = {
            "uploaded": 0,
            "error": {
                "message": "Error on image upload"
            }
        }

    return json.dumps(response)
